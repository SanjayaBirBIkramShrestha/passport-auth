const express = require('express');
const passport = require('passport')
const router = express.Router();
const bcrypt = require('bcryptjs');

// User Model
const User = require("../models/User");
const { route } = require('.');
// login page
router.get('/login', (req,res)=>res.render('login'));

// register page
router.get('/register', (req,res)=>res.render('register'));

// Register handle
router.post('/register', (req, res)=> {
    const {name, email, password, password2} = req.body;

    let errors = [];

    // check required field
    console.log("Checking inputs field...");
    if(!name || !email || !password || !password2) {
        errors.push({msg: "Please fill in all fields"})
    }
    console.log("pass...");
    

    // Check password match
    console.log("Checking password matching");
    if(password !== password2){
        errors.push({ msg: 'Passwords do not match '})
    }
    console.log("pass...");


    // Check pass length
    console.log("Checking password length");
    if(password.length < 6) {
        errors.push({ msg: "Password should be at least 6 character "})
    }
    console.log("pass...");


    if (errors.length > 0) {
        res.render('register', {
            errors,
            name,
            email,
            password,
            password2
        })
    } else {
        // Validation Passed
        User.findOne({ email: email })
        .then(user => {
            if(user) {
                // User exists
                errors.push({ msg: 'Email already exist'})
                res.render('register', {
                    errors,
                    name,
                    email,
                    password,
                    password2
                });
            } else {
                const newUser = new User({
                    name: name,
                    email: email,
                    password
                });

                // hash password
                bcrypt.genSalt(10, (err,salt)=> 
                    bcrypt.hash(newUser.password, salt, (err, hash)=> 
                    {
                        if(err) throw err;
                    // set password to hashed 
                        newUser.password = hash;
                    // save user
                    newUser.save()
                    .then(user => {
                        req.flash('success_msg', 'You are now registered and can log in')
                        console.log("redirecting to login page");
                        res.redirect('/users/login')
                    })
                }))
            }
        })
    }


    // res.send('success!!!')
})

// login handler

router.post('/login', (req, res, next) => {
    passport.authenticate('local', {
        successRedirect: '/dashboard',
        failureRedirect: 'login',
        failureFlash: true
    })(req, res, next)
});

// logout handler
router.get('/logout', (req, res)=> {
    req.logout();
    req.flash('success_msg', 'You are logged out')
    res.redirect('/users/login')
})

module.exports = router;